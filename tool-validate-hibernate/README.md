#### 验证框架使用手册
##### 功能点说明
```
1.支持所有hibernate-validate注释(不建议使用).
2.支持自定义校验,自定义错误信息
3.支持指定几个字段校验,其他字段不校验.
4.支持多个字段,混合规则校验.
```
##### maven依赖
```
jdk版本:1.7
spring版本:4.2.5.RELEASE
springboot版本:1.3.3.RELEASE
jar包版本:
<dependency>
  <groupId>com.bruce.tool</groupId>
  <artifactId>tool-validate-hibernate</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```
##### 使用样例-非空校验
```
在类的属性,或者方法的参数上增加@Function注解;
标识当前属性或者参数,有非空校验逻辑:
public class TruckReturn implements Serializable {
    @Function
    private Integer refundId;
    @Function
    private String refundNo;
}
非空校验,支持字符串和非字符串的数据对象(非封装类型的基础数据类型,不支持非空校验)
```
##### 使用样例-自定义校验
```
使用代码:
public class TruckReturn implements Serializable {
    @Functions({
        @Function,@Function
    })
    private Integer refundId;
    @Function(value = Email.class,message = Email.ERROR)
    private String email;
}
使用要求:
1.Email属于自定义校验规则类,需要实现com.bruce.tool.validate.hibernate.core.Validator<T>接口
2.校验规则和错误提示,都在Email中实现.
3.同一个属性值上多个@Function注解,可以通过@Functions实现.
```
##### 使用样例-自定义-分组校验
```
使用代码:
public class TruckReturn implements Serializable {
    @Function(value = Email.class,message = Email.ERROR,groups = Two.class)
    private Integer refundId;
    @Function(value = Email.class,message = Email.ERROR,groups = Two.class)
    private String email;
}
使用要求:
1.在自定义校验基础上,增加groups配置即可,groups配置属于hibernate-validate基础功能.
```
##### 使用样例-自定义-复杂校验
```
样例代码:
@Groups({
    @Group(value = UserInfo.class,message = UserInfo.ERROR),
    @Group(value = LoginInfo.class,message = LoginInfo.ERROR)
})
public class TruckReturn implements Serializable {
    private Integer refundId;
    private String email;
}
使用要求:
1.UserInfo以及LoginInfo需要实现com.bruce.tool.validate.hibernate.core.Validator<T>接口
2.实现的时候需要将泛型制定为具体的domain,如下:
public class UserInfo implements Validator<TruckReturn> {
    public static final String ERROR = "用户信息不完整";
    @Override
    public boolean execute(TruckReturn value) {
        // 这里实现多个字段的校验逻辑
        return false;
    }
}
```

##### 注意事项
```
1.使用自定义校验的时候,如果同时需要做非空校验,可以有2种实现方式:
    a.属性上增加@NotNull注解.
    b.规则实现类中(比如UserInfo.class),自己实现非空判断.
2.因为正则表达式比较容易出现性能问题,因此不建议在设计校验规则的时候,使用复杂正则.
```