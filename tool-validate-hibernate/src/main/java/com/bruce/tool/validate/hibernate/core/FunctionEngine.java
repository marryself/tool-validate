package com.bruce.tool.validate.hibernate.core;

import com.bruce.tool.validate.hibernate.annotation.Function;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:54 2019-04-12
 */
@Slf4j
public class FunctionEngine implements ConstraintValidator<Function,Object> {


    private Validator validator;

    @Override
    public void initialize(Function constraintAnnotation) {
        Class<? extends Validator> model = constraintAnnotation.value();
        try {
            this.validator = model.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("{}",e);
        }
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return this.validator.execute(value);
    }
}
