package com.bruce.tool.validate.hibernate.validator;

import com.bruce.tool.validate.hibernate.core.Validator;
import org.apache.commons.lang3.StringUtils;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:46 2019-04-12
 */
public class NotNull implements Validator<Object> {

    public static final String ERROR = "字段值不能为空";

    @Override
    public boolean execute(Object value) {
        if( null == value ){ return false; }
        if( value instanceof String ){
            return StringUtils.isNotBlank((String)value);
        }
        return false;
    }
}
