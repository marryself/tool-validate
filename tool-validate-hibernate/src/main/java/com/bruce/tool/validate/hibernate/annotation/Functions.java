package com.bruce.tool.validate.hibernate.annotation;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 15:14 2019-04-12
 */
@Inherited
@Documented
@Target({FIELD,PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Functions {
    Function[] value();
}