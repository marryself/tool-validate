package com.bruce.tool.validate.hibernate.annotation;

import com.bruce.tool.validate.hibernate.core.GroupEngine;
import com.bruce.tool.validate.hibernate.core.Validator;
import com.bruce.tool.validate.hibernate.validator.NotNull;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 功能 :
 * 多字段组合校验场景:
 * 1.
 * @author : Bruce(刘正航) 14:58 2019-04-12
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GroupEngine.class)
public @interface Group {

    Class<? extends Validator> value() default NotNull.class;

    String message();

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
