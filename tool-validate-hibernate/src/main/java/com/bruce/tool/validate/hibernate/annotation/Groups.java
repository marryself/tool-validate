package com.bruce.tool.validate.hibernate.annotation;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 15:14 2019-04-12
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Groups {
    Group[] value();
}
