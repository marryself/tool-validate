package com.bruce.tool.validate.hibernate.domain;

import lombok.Data;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 12:34 PM 2018/11/13
 */
@Data
public class BaseResult {
    protected String businessErrorKey;
    protected Integer code;
    protected String message;
}
