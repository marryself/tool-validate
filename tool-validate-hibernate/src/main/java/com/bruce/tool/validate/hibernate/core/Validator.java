package com.bruce.tool.validate.hibernate.core;

/**
 * 功能 :
 * 顶级校验实现
 * @author : Bruce(刘正航) 11:48 2019-04-12
 */
public interface Validator<T> {

    /**根据传入值,执行具体的值校验逻辑**/
    boolean execute(T value);

}
