package com.bruce.tool.validate.hibernate.annotation;

import com.bruce.tool.validate.hibernate.core.FunctionEngine;
import com.bruce.tool.validate.hibernate.core.Validator;
import com.bruce.tool.validate.hibernate.validator.NotNull;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:45 2019-04-12
 */
@Documented
@Target({FIELD,PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FunctionEngine.class)
public @interface Function {

    Class<? extends Validator> value() default NotNull.class;

    String message() default NotNull.ERROR;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
