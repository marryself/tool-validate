package com.juma.vms.validator;

import com.juma.vms.TruckReturn;
import com.bruce.tool.validate.hibernate.core.Validator;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 15:47 2019-04-12
 */
public class UserInfo implements Validator<TruckReturn> {

    public static final String ERROR = "用户信息不完整";

    @Override
    public boolean execute(TruckReturn value) {
        return false;
    }
}
