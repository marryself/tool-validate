package com.juma.vms;

import com.juma.vms.util.ValidUtils;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 13:55 2019-04-12
 */
public class ValidTest {

    @Test
    public void do_valid_with_common_condition() {
        TruckReturn refund = new TruckReturn();
        assertTrue(ValidUtils.valid(refund));
    }
}
