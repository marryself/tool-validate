package com.juma.vms;

import com.bruce.tool.validate.hibernate.annotation.Function;
import com.bruce.tool.validate.hibernate.annotation.Functions;
import com.bruce.tool.validate.hibernate.annotation.Group;
import com.bruce.tool.validate.hibernate.annotation.Groups;
import com.bruce.tool.validate.hibernate.validator.Email;
import com.juma.vms.group.One;
import com.juma.vms.group.Two;
import com.juma.vms.validator.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


@Groups({
    @Group(value = UserInfo.class,message = UserInfo.ERROR)
})
@Data
@ApiModel("运力退车单")
@Group(value = UserInfo.class,message = UserInfo.ERROR)
public class TruckReturn implements Serializable {
    @ApiModelProperty("退车单ID")
    @Functions({
            @Function,@Function
    })
    private Integer refundId;

    @Function(groups = One.class)
    @ApiModelProperty("退车单编号")
    private String refundNo;

    @NotNull
    @Function(groups = One.class)
    @ApiModelProperty("租户ID")
    private Integer tenantId;

    @ApiModelProperty("车牌号")
    private String plateNumber;

    @Function(value = Email.class,message = Email.ERROR,groups = Two.class)
    @ApiModelProperty("车架号")
    private String truckIdentificationNo;

    @Function(value = Email.class,message = Email.ERROR,groups = Two.class)
    @ApiModelProperty("车型")
    private Integer vehicleBoxType;

    @ApiModelProperty("箱长")
    private Integer vehicleBoxLength;

    @ApiModelProperty("车辆运营类型(1自营,2加盟,3外请,4非自营)")
    private Integer truckRunType;

    @ApiModelProperty("车辆服务范围")
    private String areaCode;

    @ApiModelProperty("关联承运商")
    private Integer vendorId;

    @ApiModelProperty("关联承运商联系电话")
    private String contactPhone;

    @ApiModelProperty("承运商类型(来源)")
    private Byte vendorType;

    @ApiModelProperty("卡车认领部门ID")
    private Integer departmentId;

    @ApiModelProperty("退车单状态(0审批中,1待入库,2已通过,3已撤销,4未通过)")
    private Integer refundStatus;

    @ApiModelProperty("退车原因类型(0司机原因,1项目原因,2强制收车)")
    private Byte refundReasonType;

    @ApiModelProperty("退车原因")
    private String refundReason;

    @ApiModelProperty("退车相关资料")
    private String refundAttachments;

    @ApiModelProperty("审核状态(0未提,1审核中,2被驳回,3已通过,4已撤销,5已放弃)")
    private Integer approvalStatus;

    @ApiModelProperty("审批意见")
    private String approvalOpinion;

    @ApiModelProperty("入库单号(SCM系统)")
    private String storageNo;

    @ApiModelProperty("工作流ID")
    private String processInstanceId;

    @ApiModelProperty("工作流提交审核时间")
    private Date submitTime;

    @ApiModelProperty("工作流-提交人")
    private Integer submitUserId;

    @ApiModelProperty("")
    private Date createTime;

    @ApiModelProperty("")
    private Integer createUserId;

    @ApiModelProperty("")
    private Date lastUpdateTime;

    @ApiModelProperty("")
    private Integer lastUpdateUserId;

}